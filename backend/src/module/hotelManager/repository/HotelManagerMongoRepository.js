"use strict";


const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));

/**
 * Clase don de se realiza toda la logica de negocio para la administracion de hoteles
 * @author Brayan Hamer Rodriguez Sanchez -  bhamerrsanchez@gmail.com
 * @copyright SmartSoft -2018
 */

class HotelManagerMongoRepository extends global.app.class.core.Service {
    /**
     * Constructor de la clase
     */
    constructor() {
        super(__filename);
        this.config = this.ConfigurationService.getConfigurationModuleConfig(this.module);

    }

    /**
     * Funcion que obtine un hotel por su id
     * @param id identificador del hotel que se desea obtener
     * @returns {Promise<any>}
     */
    getById(id) {
        return new Promise((resolve,reject)=>{
            if (data){
                reject();
            }
            resolve()
        })
    }

    /**
     * Funcion que realiza la creacion de un hotel
     * @param data - objeto con la estructura de un hotel
     * @returns {Promise<any>}
     */
    create(data) {
        return new Promise((resolve,reject)=>{
            if (data){
                reject();
            }
            resolve()
        })
    }

    /**
     * Funcion que realiza el proceso de actualizaciond de un hotel
     * @param data
     * @returns {Promise<any>}
     */
    update(data) {
        return new Promise((resolve,reject)=>{
            if (data){
                reject();
            }
            resolve()
        })
    }


    remove(data) {
        return new Promise((resolve,reject)=>{
            if (data){
                reject();
            }
            resolve()
        })
    }

}

module.exports = HotelManagerMongoRepository;
