"use strict";


const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));

/**
 * Clase don de se realiza toda la logica de negocio para la administracion de hoteles
 * @author Brayan Hamer Rodriguez Sanchez -  bhamerrsanchez@gmail.com
 * @copyright SmartSoft -2018
 */

class HotelManagerRepository extends global.app.class.core.Service {
    /**
     * Constructor de la clase
     */
    constructor() {
        super(__filename);
        this.config = this.ConfigurationService.getConfigurationModuleConfig(this.module);

    }

    /**
     * Funcion que realiza el proceso de la obtencion de los datos configurados
     * los recurso del modulo con el atributo hotel
     * @returns {Promise<T>}
     */
    get() {
        log.debug(`Obteniendo lista de hoteles configurados en los recursos del modulo`);
        return new Promise((resolve, reject) => {
            log.trace(`Cantidad de hoteles obtenidos : ${this.config.hotels.length}`);
            resolve(this.config.hotels || []);
        })
    }

}

module.exports = HotelManagerRepository;
