"use strict";

const HotelManagerController = require('./HotelManagerController');
const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));

/**
 * Clase donde se realiza la definicion de los recursos rest para gestionar hoteles
 * @author Brayan Hamer Rodriguez Sanchez -  bhamerrsanchez@gmail.com
 * @copyright SmartSoft -2018
 */

class HotelManagerRestController extends global.app.class.core.Controller {
    /**
     * Constructor de la clase
     */
    constructor() {
        super(__filename);
        this._hotelManagerController = new HotelManagerController();

    }

    createRoutes() {
        //recurso para consultar los hoteles almacenados
        global.router.get('/hotel', (req, res) => {
            log.info(`petición al recurso  /hotel metodo get`);
            try {
                this.get(req).then((result) => {
                    res.status(200).json(result);
                }).catch((error)=>{
                    res.status(error.status, error.msg);
                });
            } catch (error) {
                res.status(error.status, error.msg);
            }
        });

        //recurso para realizar la cracion de un  hotele
        global.router.post('/hotel', (req, res) => {
            try {
                this.startServices(req, res);
                res.status(200).json({});
            } catch (error) {
                res.status(error.status, error.msg);
            }
        });

        //recurso que detiene los servicios de las aplicaciones de smartsoft
        global.router.put('/hotel', (req, res) => {
            try {
                this.update(req, res);
                res.status(200).json({});
            } catch (error) {
                res.status(error.status, error.msg);
            }

        });

        //recurso que elimina los servicios de las aplicaciones de smartsoft
        global.router.delete('/hotel', (req, res) => {
            try {
                this.remove(req);
                res.status(200).json({});
            } catch (error) {
                res.status(error.status, error.msg);
            }

        });

    }

    get(req) {
        let data = req.query;
        log.trace(`parametros ${JSON.stringify(data)}`);
        return this._hotelManagerController.get(data);
    }

    create(req) {
        let data = req.body.data;
        log.trace(`parametros ${JSON.stringify(data)}`);
        return this._hotelManagerController.create(data)
    }

    update(req) {
        let data = req.body.data;
        log.trace(`parametros ${JSON.stringify(data)}`);
        return this._hotelManagerController.update(data);
    }

    remove(req) {
        let data = req.body.data;
        log.trace(`parametros ${JSON.stringify(data)}`);
        return this._hotelManagerController.remove(data);
    }

}

module.exports = HotelManagerRestController;
