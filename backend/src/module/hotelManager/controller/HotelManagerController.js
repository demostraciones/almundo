"use strict";

const HotelManagerService = require('../service/HotelManagerService');
const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));

/**
 * COntrolador general que expone las operaciones de gestion de hoteles
 * @author Brayan Hamer Rodriguez Sanchez -  bhamerrsanchez@gmail.com
 * @copyright SmartSoft -2018
 */

class HotelManagerController extends global.app.class.core.Controller {
    /**
     * Constructor de la clase
     */
    constructor() {
        super(__filename);
        this._hotelManagerService = new HotelManagerService();
    }

    /**
     * Metodo que expone la funcionalidad de obtener una lista de hoteles por filtro
     * @param data parametros que aplican como filtro
     * @returns {Promise<T>}
     */
    get(data) {
        if (!data.hasOwnProperty('stars')) {
            data.stars = [];
        }
        return this._hotelManagerService.get(data);
    }

    /**
     * Metodo que expone la funcionalidad de creacion de un hotel
     * @param data - parametro que contien toda la estructura de un hotel
     * @returns {Promise<any>}
     */
    create(data) {
        return this._hotelManagerService.create(data);
    }

    /**
     * Metodo que expone la funcionalidad de actualizaciond e un hotel
     * @param data - objeto que contiene el id y los parametros quese desean actualizar dle hotel
     * @returns {Promise<any>}
     */
    update(data) {
        return this._hotelManagerService.update(data);
    }

    /**
     * Metodo que expone la funcionalidad de eliminacion de un hotel
     * @param data - parametro que indica el id del hotel a ser eliminado
     * @returns {Promise<T>}
     */
    remove(data) {
        return this._hotelManagerService.remove(data);
    }
}

module.exports = HotelManagerController;
