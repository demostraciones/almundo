"use strict";


const HotelManagerRepository = require('../repository/HotelManagerRepository');
const HotelManagerMongoRepository = require('../repository/HotelManagerMongoRepository');

const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));

/**
 * Clase don de se realiza toda la logica de negocio para la administracion de hoteles
 * @author Brayan Hamer Rodriguez Sanchez -  bhamerrsanchez@gmail.com
 * @copyright SmartSoft -2018
 */

class HotelManagerService extends global.app.class.core.Service {
    /**
     * Constructor de la clase
     */
    constructor() {
        super(__filename);
        this._serviceSystemRepository = new HotelManagerRepository();
        this._hotelManagerMongoRepository = new HotelManagerMongoRepository();

    }

    /**
     * Servicio utilizado para obtener los hoteles fiiltrados
     * @param data - objeto donde indica el filtro de los datos por el atributo name y starts de una estructura de tipo hotel
     * @returns {Promise<T>}
     */
    get(data) {
        return this._serviceSystemRepository.get(data)
            .then((result) => {
                if (data.hasOwnProperty('name') && data.name.length > 0) {
                    result = result.filter((value) => {
                        return value.name.toUpperCase().includes(data.name.toUpperCase());
                    });
                }
                if (data.hasOwnProperty('stars') && data.stars.length > 0) {
                    result = result.filter((value) => {
                        return data.stars.includes(value.stars.toString());
                    });
                }
                log.info(`cantidad de resultados obtenidos : ${result.length}`);
                return result;
            })
            .catch(err => log.error(err));
    }

    /**
     * Metodo que realiza el proceso de creacion de un hotel
     * @param data
     * @returns {Promise<any>}
     */
    create(data) {
        return this._hotelManagerMongoRepository.create(data)
            .then((result) => {
                log.trace(result);
                return result;
            })
            .catch(err => log.error(err));
    }

    /**
     * Metodo que realiza el proceso de actualizacion de un hotel
     * @param data
     * @returns {Promise<any>}
     */
    update(data) {
        return this._hotelManagerMongoRepository.update(data)
            .then((result) => {
                log.trace(result);
                return result;
            })
            .catch(err => log.error(err));
    }

    /**
     * Metodo que realiza el proceso de elimiancion de un hotel
     * @param data
     * @returns {Promise<T>}
     */
    remove(data) {
        return this._hotelManagerMongoRepository.remove(data)
            .then((result) => {
                log.trace(result);
                return result;
            })
            .catch(err => log.error(err));
    }

}

module.exports = HotelManagerService;
