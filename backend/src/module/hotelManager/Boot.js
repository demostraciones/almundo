"use strict";
/**
 * Clase que realiza la configuracion y carga del las funcionalidades del modulo
 * @author Brayan Hamer ROdriguez Sanchez - bhamerrsanchez@gmail.com
 * @copyright SmartSoft -2017
 */

/**
 * Definición de la variable de registro de logs
 * @type {Logger}
 */
const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));

const HotelManagerRestController = require('./controller/HotelManagerRestController');

class Boot {
    /**
     * Constructor de la clase
     */
    constructor() {
        log.info(`Iniciando modulo`);
        this._hotelManagerRestController = new HotelManagerRestController();
    }

    /**
     * Función de configuracion del modulo
     */
    setup() {
        log.info(`Realizando congfiguracion y carga del modulo HotelManager`);
        this._hotelManagerRestController.createRoutes();
    }
}

module.exports = Boot;
