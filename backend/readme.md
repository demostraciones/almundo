# Pasos de configuración y ejecución del proyecto

1. Actualización del framework

    el poryecto contiene un submodulo de git que es el framework, con el fin de descargar el poryecto del
    framework se debe ejecutar el siguiente comando con git, ubicado sobre la ruta del backend
    
    ```
      git submodule update --init --remote
    ```
2. descarga de paquetes

    Ejecutar los siguientes comandos
    ```
    cd backend
    npm install
    ```
3. Ejecución del proyecto

* Desarrollo

    ```
    cd backend
    npm start
    ```
    
* producccion

    ```
    cd backend
    npm run start:prod
    ```