const API_PROTOCOL = 'http://';
const API_HOST = 'localhost';
const API_PORT = '9500';
const API_PATH = '/almundo';
const API_VERSION = '/1.0';
const API_URL = `${API_PROTOCOL}${API_HOST}:${API_PORT}${API_PATH}${API_VERSION}`;
const path = {
    api: {
        hotel: {
            base: API_URL + "/hotel",
        }
    }
};

export default {
    path: path
};
