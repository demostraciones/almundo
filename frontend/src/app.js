/**
 * Seccion de carga de las depndencias de css del proyecto
 */
import 'bootstrap/dist/css/bootstrap.css';
import './assets/fonts/almundo/almundo-font-family.css';
import './styles/almundo.css';
/**
 * Carga de las dependencias js del poryecto
 */
import 'bootstrap';
import angular from 'angular';
import uirouter from '@uirouter/angularjs/release/angular-ui-router.min';
import routing from './appConfig';

// Sentencia utilizada para la generacion de la representacion base64 del logo
// let logo = require('./assets/images/logo-almundo.svg');
// console.log(logo);
import moduleHotel from './module/hotel';

/**
 * Creacion del modulo de angular
 */
let app = angular.module('almundoApp',
    [
        uirouter,
        moduleHotel
    ]);
app.config(routing);