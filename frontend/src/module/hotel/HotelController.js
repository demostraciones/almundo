let img = require('../../assets/images/hotels/9817258_55_b.jpg');
export default class HotelController {
    /**
     * Constructor del controlador
     * @param hotelService
     */
    constructor(hotelService) {
        this.hotelService = hotelService;

        this.name = undefined;
        this.stars = [];
        this.allStars = false;
        this.fiveStars = false;
        this.fourStars = false;
        this.threeStars = false;
        this.twoStars = false;
        this.oneStars = false;

        this.hotels = [];
        this.img = img;
        angular.element(document).ready(()=> {
            this.get();
        });
    }

    /**
     * Metodo para obtener los hotelws
     */
    get() {
        this.stars=[];
        if (this.fiveStars ) this.stars.push(5);
        if (this.fourStars ) this.stars.push(4);
        if (this.threeStars ) this.stars.push(3);
        if (this.twoStars ) this.stars.push(2);
        if (this.oneStars ) this.stars.push(1);
        let data = {
            name: this.name || "",
                stars: this.stars || ""
        };
        this.hotelService.get(data).then(
            (response) => {
                this.hotels = response.data;
                this.hotels.forEach((value)=>{
                    value.srcImg = require('../../assets/images/hotels/'+value.image);
                })
                console.log(this.hotels);
            },
            function (error) {
                console.error(error.message);
            });
    };

    validateStartCheck(){
        this.fiveStars = this.allStars;
        this.fourStars = this.allStars;
        this.threeStars = this.allStars;
        this.twoStars = this.allStars;
        this.oneStars = this.allStars;
    }
}

HotelController
    .$inject = ['HotelService'];
