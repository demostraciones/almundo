routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
    $stateProvider
        .state('hotel', {
            url: '/',
            template: require('./view.html'),
            controller: 'HotelController',
            controllerAs: 'hotel'
        });
}