import angular from 'angular';
import config from '../../apiConfig';
class HotelService {
    constructor(http) {
        this.http = http;
        this.config = config;
        console.log(`Cargando servicio hotelService
        config : ${JSON.stringify(this.config)}
        `);
    }

    /**
     * Servicio que se comunica con el api para obtener los
     * datos por los parametros indicados
     * @param data - contiene los parametros para realizar los filtros sobre el api
     * @returns {*}
     */
    get(data) {
        return this.http({
            method : 'GET',
            url : this.config.path.api.hotel.base,
            params : data
        });
    }
}

HotelService.$inject = ['$http'];

export default angular.module('moduleHotel.HotelService',[])
    .service('HotelService', HotelService)
    .name;