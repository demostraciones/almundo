import angular from 'angular';
import uirouter from '@uirouter/angularjs/release/angular-ui-router.min';

import routing from './routes';
import HotelController from './Hotelcontroller';
import HotelService from './HotelService';

export default angular.module('moduleHotel', [uirouter, HotelService])
    .config(routing)
    .controller('HotelController', HotelController)
    .name;