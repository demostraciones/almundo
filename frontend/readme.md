# Pasos de configuración y ejecución del proyecto

1. descarga de paquetes

    Ejecutar los siguientes comandos
    ```
    cd frontend
    npm install
    ```
3. Ejecución del proyecto

* Servidor de Desarrollo : despliegue de proyecto en servidor de desarrollo local

    ```
    cd frontend
    npm run server:dev
    ```
    
* Compilación desarrollo

    ```
    cd frontend
    npm run build:dev
    ```
    
* Compilación produccción

    ```
    cd frontend
    npm run build:prod
    ```